require 'savon'
require 'fm_timbrado_cfdi/fm_respuesta'

module FmTimbradoCfdi
  class FmCliente
    #attrs
    attr_accessor :user_id, :user_pass, :namespace, :fm_wsdl, :endpoint, :ssl_verify_mod, :log, :log_level
    
    def initialize
      # La configuracion por default es la del ambiente de pruebas de FM
      # Datos de acceso al webservice 
      @user_id = 'UsuarioPruebasWS'
      @user_pass = 'b9ec2afa3361a59af4b4d102d3f704eabdf097d4'
      # Datos del webservide de prueba
      @namespace = "https://t2demo.facturacionmoderna.com/timbrado/soap"
      @endpoint = "https://t2demo.facturacionmoderna.com/timbrado/soap"
      @fm_wsdl = "https://t2demo.facturacionmoderna.com/timbrado/wsdl"

      #Opciones adicionales
      @log = false
      @log_level = :error
      @ssl_verify_mod = :none
    end

    def peticion_timbrar(rfc_emisor, documento, generar_timbre = true, generar_pdf = false, generar_cbb = false)
      text_to_cfdi = Base64::encode64( documento )
      # Realizamos la peticion
      configurar_cliente
      #Here geneerates the soap through  savon
      #@client = FmCliente.new #call is a savon method, #request_timbrar_cfdi is an available operation to server, #message
      response = @client.call( :request_timbrar_cfdi,
        #soap.namespace = @namespace
        message: { "param0" => {
          "UserPass" => user_pass,
          "UserID" => user_id, 
          "emisorRFC" => rfc_emisor,
          "text2CFDI" => text_to_cfdi,
          "generarTXT" => generar_timbre,
          "generarPDF" => generar_pdf,
          "generarCBB" => generar_cbb,
        } }
      ) # response
      FmRespuesta.new(response)
    end #peticion timbrar
    
    def peticion_cancelar(uuid, rfc_emisor)

      #text_to_cfdi = Base64::encode64( documento )
      # Realizamos la peticion
      configurar_cliente
      response = @client.call( :request_cancelar_cfdi,
        #soap.namespace = @namespace
        message: { "param0" => {
          "UserPass" => user_pass,
          "UserID" => user_id, 
          "emisorRFC" => rfc_emisor,
          "uuid" => uuid   
        } }
      ) # response
      FmRespuesta.new(response)
      
    end    

 

    #Envia Sello digital a FM
    def enviar_csd(rfc_emisor, archivo_cer, archivo_key, key_pass)
      configurar_cliente
      response = @client.call( :activar_cancelacion,
        #soap.namespace = @namespace
        message: { "param0" => {
          "UserPass" => user_pass,
          "UserID" => user_id, 
          "emisorRFC" => rfc_emisor,
          "archivoCer" => archivo_cer,   
          "archivoKey" => archivo_key,
          "clave" => key_pass
        } }
      ) # response

      FmRespuesta.new(response)
    end 
    

    private
    def configurar_cliente
      # Configuración de Savon
      # Savon.configure do |config|
      #   config.raise_errors = false 
      #   config.log_level = log_level 
      #   config.log = log 
      # end

      @client  = Savon.client(
        ssl_verify_mode: :none,
        #wsdl.document = fm_wsdl
        #wsdl.endpoint = endpoint
        :wsdl => fm_wsdl
        #endpoint endpoint
      )
      HTTPI.log =  log
    end

  end #class
end#module
